package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomeElementMap {
		
	
	@FindBy(xpath="//a[@class='fancybox-item fancybox-close']")
	protected WebElement closeBanner;
	
	@FindBy(xpath="//button[@id='menu-icon-trigger']")
	protected WebElement menuSanduiche;
	
	@FindBy(xpath="//div[@class='img-container']")
	protected WebElement logoMenuSanduiche;

}
