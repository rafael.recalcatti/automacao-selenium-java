package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.PageFactory;

import util.SetUp;;

public class HomePage extends HomeElementMap{

	
	public  HomePage() {
		
		PageFactory.initElements(SetUp.getDriver(), this);
	}
	
	public void fecharBanner() {
		//tratar falha aki
		closeBanner.click();
		
	}
	
	public void clicarMenu() {
		
		JavascriptExecutor js = (JavascriptExecutor) SetUp.getDriver();
		js.executeScript(
				"document.getElementById('menu-icon-trigger').click();");
		//menuSanduiche.click();
	}
	
	public boolean validaMenu() {
				
	return	logoMenuSanduiche.isDisplayed();
	
	}
	
}
