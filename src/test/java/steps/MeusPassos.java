package steps;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import cucumber.api.PendingException;
import cucumber.api.java.es.Dado;
import cucumber.api.java.gl.E;
import cucumber.api.java.it.Quando;
import cucumber.api.java.pt.Entao;
import pages.HomePage;
import util.SetUp;

public class MeusPassos{
	
	static WebDriver driver;

@Dado("^Acessei a pagina da zero$")
public void acessei_a_pagina_da_zero() throws Throwable {
   
	//SetUp setUp;
    driver = SetUp.getDriver();
    driver.navigate().to("http://www.zero-defect.com.br");
   
}

@E("^fechei o bannerBF$")
public void fechei_o_bannerBF() throws Throwable {

	HomePage homePage = new HomePage();
	homePage.fecharBanner();
	
}

@Quando("^cliquei no menu sanduiche$")
public void cliquei_no_menu_sanduiche() throws Throwable {
	HomePage homePage = new HomePage();
	homePage.clicarMenu();
}

@Entao("^abriu o menu$")
public void abriu_o_menu() throws Throwable {
	HomePage homePage = new HomePage();	
	Assert.assertTrue(homePage.validaMenu());
}

}
