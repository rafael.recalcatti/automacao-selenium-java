package util;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class SetUp {

	static WebDriver driver;

	@Before
	public void setUP() {

		try {

			System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			setDriver(driver);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public static WebDriver getDriver() {
		return driver;
	}

	public static void setDriver(WebDriver driver) {
		SetUp.driver = driver;
	}

	@After
	public void print(Scenario cenario) {

		try {
			
			byte[] screenshot = ((TakesScreenshot) SetUp.getDriver()).getScreenshotAs(OutputType.BYTES);
			cenario.embed(screenshot, "image/png");
			
		} catch (UnsupportedOperationException somePlatformsDontSupportScreenshots) {
			System.err.println("Erro Print" + somePlatformsDontSupportScreenshots.getMessage());

		} catch (WebDriverException e) {
			cenario.write("WARNING. Failed take screenshots with exception:" + e.getMessage());
		}
		
		SetUp.getDriver().quit();
	}
	
	

}
